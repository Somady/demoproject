﻿using Microsoft.AspNetCore.Identity;

namespace DemoProject.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string FullName { get; set; }
        public DateTime CreateDate { get; set; } = DateTime.Now;
    }
}
