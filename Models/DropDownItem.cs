﻿namespace DemoProject.Models
{
    public class DropDownItem
    {
        public int Value { get; set; }
        public string Text { get; set; }
    }
}
