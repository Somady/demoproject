﻿namespace DemoProject.Models
{
    public class PlayerModel
    {
        public long PlayerId { get; set; }
        public string PlayerName { get; set; }
        public int JerseyNumber { get; set; }
        public long PositionId { get; set; }
        public string PositionName { get; set; }
        public long NationalityId { get; set; }
        public string CountryName { get; set; }
        public DateTime? BirthDate { get; set; }
        public int? Age { get; set; }
        public string BirthPlace { get; set; }
        public int? Weight { get; set; }
        public int? Height { get; set; }
        public DateTime? ClubDebutDate { get; set; }
        public string QualityDescription { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
    }
}
