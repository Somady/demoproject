﻿$(document).ready(function () {
    $.ajax({
        type: "GET",
        url: '/Player/GetDropdownPositionData',
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        data: {},
        success: function (result)
        {
            $.each(result, function (i) {
                $('#cboPosition').append($('<option></option>').val(result[i].value).html(result[i].text));
            });
        },
        error: function (result) {
            alert('Error loading data.');
        }
    });


    //Load Countries
    $.ajax({
        type: "GET",
        url: '/Player/GetDropdownCountryData',
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        data: {},
        success: function (result) {
            $.each(result, function (i) {
                $('#cboNationality').append($('<option></option>').val(result[i].value).html(result[i].text));
            });
        },
        error: function (result) {
            alert('Error loading data.');
        }
    });


    $('#example tbody').on('click', 'td #btnEdit', function () {
        var grid = $("#example").DataTable();
        rowData = grid.row($(this).closest('tr')).data();
        inRow = grid.row($(this).closest('tr'));

        var _id = grid.row($(this).closest('tr')).data().playerId;

        $('#cboPosition').val(rowData.positionId);
        $('#cboNationality').val(rowData.nationalityId);
    })
})