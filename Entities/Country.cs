﻿using System;
using System.Collections.Generic;

namespace DemoProject.Entities;

public partial class Country
{
    public long CountryId { get; set; }

    public string CountryName { get; set; }
}
