﻿using System;
using System.Collections.Generic;

namespace DemoProject.Entities;

public partial class PreviousClub
{
    public long PreviousClubsId { get; set; }

    public string PreviousClubsName { get; set; }

    public long PlayerId { get; set; }
}
