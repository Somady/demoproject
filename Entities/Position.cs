﻿using System;
using System.Collections.Generic;

namespace DemoProject.Entities;

public partial class Position
{
    public long PositionId { get; set; }

    public string PositionName { get; set; }
}
