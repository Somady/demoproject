﻿namespace DemoProject.Services.Position
{
    public interface IPositionService
    {
        Task<bool> PositionExistsAsync(long Id);
        Task AddPositionAsync(Entities.Position entity);
        Task<IEnumerable<Entities.Position>> GetAllPositionsAsync();
        Task<Entities.Position> GetPositionByIdAsync(long Id);
        Task UpdatePositionAsync(Entities.Position entity);
        Task DeletePositionAsync(long Id);
    }
}
