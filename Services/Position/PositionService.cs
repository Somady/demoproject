﻿using DemoProject.Entities;
using Microsoft.EntityFrameworkCore;

namespace DemoProject.Services.Position
{
    public class PositionService : IPositionService
    {
        private readonly FcbDemoDbContext _context;

        public PositionService(FcbDemoDbContext context)
        {
            _context = context;
        }

        public async Task AddPositionAsync(Entities.Position entity)
        {
            await _context.AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public async Task DeletePositionAsync(long Id)
        {
            var entity = await _context.Positions.SingleOrDefaultAsync(c => c.PositionId == Id);
            _context.Remove(entity);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Entities.Position>> GetAllPositionsAsync()
        {
            var list = await _context.Positions.ToListAsync();
            return list;
        }

        public async Task<Entities.Position> GetPositionByIdAsync(long Id)
        {
            var list = await _context.Positions.Where(c => c.PositionId == Id).FirstOrDefaultAsync();
            return list;
        }

        public async Task<bool> PositionExistsAsync(long Id)
        {
            return await _context.Positions.AnyAsync(c => c.PositionId == Id);
        }

        public async Task UpdatePositionAsync(Entities.Position entity)
        {
            var model = await _context.Positions.Where(u => u.PositionId == entity.PositionId).FirstOrDefaultAsync();

            model.PositionId = entity.PositionId;
            model.PositionName = entity.PositionName;

            _context.Positions.Update(model);
            await _context.SaveChangesAsync();
        }
    }
}
