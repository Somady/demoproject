﻿using DemoProject.Models;

namespace DemoProject.Services.Player
{
    public interface IPlayerService
    {
        Task<IEnumerable<Entities.Position>> GetAllPositionsAsync();
        Task<IEnumerable<Entities.Country>> GetAllCountriesAsync();
        Task<bool> PlayerExistsAsync(long Id);
        Task AddPlayerAsync(Entities.Player entity);
        //Task<IEnumerable<Entities.Player>> GetAllPlayersAsync();
        Task<IEnumerable<PlayerModel>> GetAllPlayersAsync();
        Task<Entities.Player> GetPlayerByIdAsync(long Id);
        Task UpdatePlayerAsync(Entities.Player entity);
        Task DeletePlayerAsync(long Id);
    }
}
