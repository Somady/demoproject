﻿using DemoProject.Entities;
using DemoProject.Models;
using Microsoft.EntityFrameworkCore;

namespace DemoProject.Services.Player
{
    public class PlayerService : IPlayerService
    {
        private readonly FcbDemoDbContext _context;

        public PlayerService(FcbDemoDbContext context)
        {
            _context = context;
        }

        public async Task AddPlayerAsync(Entities.Player entity)
        {
            await _context.AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public async Task DeletePlayerAsync(long Id)
        {
            var entity = await _context.Players.SingleOrDefaultAsync(c => c.PlayerId == Id);
            _context.Remove(entity);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Country>> GetAllCountriesAsync()
        {
            var list = await _context.Countries.ToListAsync();
            return list;
        }

        public async Task<IEnumerable<PlayerModel>> GetAllPlayersAsync()
        {
            var list = await (from pl in _context.Players
                        join c in _context.Countries
                        on pl.NationalityId equals c.CountryId
                        join po in _context.Positions
                        on pl.PositionId equals po.PositionId
                        select new PlayerModel
                        {
                            PlayerId = pl.PlayerId,
                            PlayerName = pl.PlayerName,
                            JerseyNumber = pl.JerseyNumber,
                            PositionId = pl.PositionId,
                            PositionName = po.PositionName,
                            NationalityId = pl.NationalityId,
                            CountryName = c.CountryName,
                            BirthDate = pl.BirthDate,
                            Age = pl.Age,
                            BirthPlace = pl.BirthPlace,
                            Weight = pl.Weight,
                            Height = pl.Height,
                            ClubDebutDate = pl.ClubDebutDate,
                            QualityDescription = pl.QualityDescription,
                            CreateDate = pl.CreateDate,
                            CreateBy = pl.CreateBy
                        }).ToListAsync();

            return list;
        }

        //public async Task<IEnumerable<Entities.Player>> GetAllPlayersAsync()
        //{
        //    var list = await _context.Players.ToListAsync();
        //    return list;
        //}

        public async Task<IEnumerable<Entities.Position>> GetAllPositionsAsync()
        {
            var list = await _context.Positions.ToListAsync();
            return list;
        }

        public async Task<Entities.Player> GetPlayerByIdAsync(long Id)
        {
            var list = await _context.Players.Where(c => c.PlayerId == Id).FirstOrDefaultAsync();
            return list;
        }

        public async Task<bool> PlayerExistsAsync(long Id)
        {
            return await _context.Players.AnyAsync(c => c.PositionId == Id);
        }

        public async Task UpdatePlayerAsync(Entities.Player entity)
        {
            var model = await _context.Players.Where(u => u.PlayerId == entity.PlayerId).FirstOrDefaultAsync();

            model.PlayerId = entity.PlayerId;
            model.PlayerName = entity.PlayerName;
            model.JerseyNumber = entity.JerseyNumber;
            model.PositionId = entity.PositionId;
            model.NationalityId = entity.NationalityId;
            model.BirthDate = entity.BirthDate;
            model.BirthPlace = entity.BirthPlace;
            model.Weight = entity.Height;
            model.ClubDebutDate = entity.ClubDebutDate;
            model.QualityDescription = entity.QualityDescription;
            model.CreateDate = entity.CreateDate;
            model.CreateBy = entity.CreateBy;

            _context.Players.Update(model);
            await _context.SaveChangesAsync();
        }
    }
}
