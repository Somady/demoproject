﻿using DemoProject.Entities;
using DemoProject.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;

namespace DemoProject.Services.Users
{
    public class UserService : IUserService
    {
        private readonly FcbDemoDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public UserService(FcbDemoDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task AddUserAsync(AspNetUser entity)
        {
            var appUser = new ApplicationUser()
            {
                UserName = entity.UserName,
                Email = entity.Email,
                PhoneNumber = entity.PhoneNumber,
                FullName = entity.FullName,
                CreateDate = DateTime.Now
            };

            IdentityResult result = await _userManager.CreateAsync(appUser, entity.PasswordHash);

            if (result.Succeeded)
            {
                var claims = new List<Claim>
                    {
                        new Claim("GivenName", appUser.UserName),
                        new Claim("FamilyName", appUser.FullName),
                        new Claim("WebSite", appUser.Id),
                        new Claim("Email", appUser.Email),
                        new Claim("PhoneNumber", appUser.PhoneNumber)
                };

                await _userManager.AddClaimsAsync(appUser, claims);
            }
            else
            {
                throw new Exception(result.Errors.ToString());
            }
        }

        public async Task DeleteUserAsync(string id)
        {
            var entity = await _context.AspNetUsers.SingleOrDefaultAsync(c => c.Id == id);
            _context.Remove(entity);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<AspNetUser>> GetAllUsersAsync()
        {
            var list = await _context.AspNetUsers.ToListAsync();

            return list;
        }

        public async Task<AspNetUser> GetUserByIdAsync(string id)
        {
            var list = await _context.AspNetUsers.SingleOrDefaultAsync(x => x.Id == id);

            return list;
        }

        public async Task UpdateUserAsync(AspNetUser entity)
        {
            var data = await _context.AspNetUsers
                .Where(u => u.Id == entity.Id)
                .FirstOrDefaultAsync();

            data.Id = entity.Id;
            data.FullName = entity.FullName;
            data.UserName = entity.UserName;
            data.NormalizedUserName = entity.UserName.ToUpper();
            data.Email = entity.Email;
            data.NormalizedEmail = entity.Email.ToUpper();
            data.PhoneNumber = entity.PhoneNumber;
            data.CreateDate = DateTime.Now;

            _context.AspNetUsers.Update(data);
            await _context.SaveChangesAsync();
        }
    }
}
