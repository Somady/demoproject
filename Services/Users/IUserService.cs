﻿using DemoProject.Entities;

namespace DemoProject.Services.Users
{
    public interface IUserService
    {
        Task AddUserAsync(AspNetUser entity);
        Task<IEnumerable<AspNetUser>> GetAllUsersAsync();
        Task<AspNetUser> GetUserByIdAsync(string id);
        Task UpdateUserAsync(AspNetUser entity);
        Task DeleteUserAsync(string id);
    }
}
