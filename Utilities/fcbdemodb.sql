USE [master]
GO
/****** Object:  Database [FCB_Demo_DB]    Script Date: 11/9/2023 10:47:14 PM ******/
CREATE DATABASE [FCB_Demo_DB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'FCB_Demo_DB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.SQL2022\MSSQL\DATA\FCB_Demo_DB.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'FCB_Demo_DB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.SQL2022\MSSQL\DATA\FCB_Demo_DB_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT, LEDGER = OFF
GO
ALTER DATABASE [FCB_Demo_DB] SET COMPATIBILITY_LEVEL = 160
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [FCB_Demo_DB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [FCB_Demo_DB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [FCB_Demo_DB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [FCB_Demo_DB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [FCB_Demo_DB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [FCB_Demo_DB] SET ARITHABORT OFF 
GO
ALTER DATABASE [FCB_Demo_DB] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [FCB_Demo_DB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [FCB_Demo_DB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [FCB_Demo_DB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [FCB_Demo_DB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [FCB_Demo_DB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [FCB_Demo_DB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [FCB_Demo_DB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [FCB_Demo_DB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [FCB_Demo_DB] SET  ENABLE_BROKER 
GO
ALTER DATABASE [FCB_Demo_DB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [FCB_Demo_DB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [FCB_Demo_DB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [FCB_Demo_DB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [FCB_Demo_DB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [FCB_Demo_DB] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [FCB_Demo_DB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [FCB_Demo_DB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [FCB_Demo_DB] SET  MULTI_USER 
GO
ALTER DATABASE [FCB_Demo_DB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [FCB_Demo_DB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [FCB_Demo_DB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [FCB_Demo_DB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [FCB_Demo_DB] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [FCB_Demo_DB] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [FCB_Demo_DB] SET QUERY_STORE = ON
GO
ALTER DATABASE [FCB_Demo_DB] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 1000, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
USE [FCB_Demo_DB]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 11/9/2023 10:47:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoleClaims]    Script Date: 11/9/2023 10:47:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoleClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 11/9/2023 10:47:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](256) NULL,
	[NormalizedName] [nvarchar](256) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 11/9/2023 10:47:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 11/9/2023 10:47:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](450) NOT NULL,
	[ProviderKey] [nvarchar](450) NOT NULL,
	[ProviderDisplayName] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 11/9/2023 10:47:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](450) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 11/9/2023 10:47:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](450) NOT NULL,
	[FullName] [nvarchar](max) NULL,
	[CreateDate] [datetime2](7) NOT NULL,
	[UserName] [nvarchar](256) NULL,
	[NormalizedUserName] [nvarchar](256) NULL,
	[Email] [nvarchar](256) NULL,
	[NormalizedEmail] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEnd] [datetimeoffset](7) NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
 CONSTRAINT [PK_AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserTokens]    Script Date: 11/9/2023 10:47:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserTokens](
	[UserId] [nvarchar](450) NOT NULL,
	[LoginProvider] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[LoginProvider] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Country]    Script Date: 11/9/2023 10:47:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Country](
	[CountryId] [bigint] IDENTITY(1,1) NOT NULL,
	[CountryName] [nvarchar](50) NULL,
 CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED 
(
	[CountryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Player]    Script Date: 11/9/2023 10:47:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Player](
	[PlayerId] [bigint] IDENTITY(1,1) NOT NULL,
	[PlayerName] [nvarchar](100) NOT NULL,
	[JerseyNumber] [int] NOT NULL,
	[PositionId] [bigint] NOT NULL,
	[NationalityId] [bigint] NOT NULL,
	[BirthDate] [datetime] NULL,
	[Age]  AS (datediff(year,[BirthDate],getdate())),
	[BirthPlace] [nvarchar](100) NULL,
	[Weight] [int] NULL,
	[Height] [int] NULL,
	[ClubDebutDate] [date] NULL,
	[QualityDescription] [nvarchar](max) NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [nvarchar](50) NULL,
 CONSTRAINT [PK_Player] PRIMARY KEY CLUSTERED 
(
	[PlayerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Position]    Script Date: 11/9/2023 10:47:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Position](
	[PositionId] [bigint] IDENTITY(1,1) NOT NULL,
	[PositionName] [nvarchar](50) NULL,
 CONSTRAINT [PK_Position] PRIMARY KEY CLUSTERED 
(
	[PositionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PreviousClubs]    Script Date: 11/9/2023 10:47:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PreviousClubs](
	[PreviousClubsId] [bigint] IDENTITY(1,1) NOT NULL,
	[PreviousClubsName] [nvarchar](50) NOT NULL,
	[PlayerId] [bigint] NOT NULL,
 CONSTRAINT [PK_PreviousClubs] PRIMARY KEY CLUSTERED 
(
	[PreviousClubsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20231108121338_InitialMigration', N'7.0.13')
GO
SET IDENTITY_INSERT [dbo].[AspNetUserClaims] ON 
GO
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (6, N'cfab9e2d-064c-46fa-8954-905a0dca6c21', N'GivenName', N'Admin')
GO
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (7, N'cfab9e2d-064c-46fa-8954-905a0dca6c21', N'FamilyName', N'Somad Yessoufou')
GO
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (8, N'cfab9e2d-064c-46fa-8954-905a0dca6c21', N'WebSite', N'cfab9e2d-064c-46fa-8954-905a0dca6c21')
GO
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (9, N'cfab9e2d-064c-46fa-8954-905a0dca6c21', N'Email', N'yessouf2009@yahoo.fr')
GO
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (10, N'cfab9e2d-064c-46fa-8954-905a0dca6c21', N'PhoneNumber', N'0245508217')
GO
SET IDENTITY_INSERT [dbo].[AspNetUserClaims] OFF
GO
INSERT [dbo].[AspNetUsers] ([Id], [FullName], [CreateDate], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount]) VALUES (N'cfab9e2d-064c-46fa-8954-905a0dca6c21', N'Somad Yessoufou', CAST(N'2023-11-09T21:14:17.6110582' AS DateTime2), N'Admin', N'ADMIN', N'yessouf2009@yahoo.fr', N'YESSOUF2009@YAHOO.FR', 0, N'AQAAAAIAAYagAAAAEM18+VFlhnbhMZB0xNMweTg01Y2IWUksBLFPUdkrAx1DczJp+q1yyGGyVSe2Rhb8pQ==', N'U4RZYWXF4UWGPGMUJAHLVOICQ4LV7CRU', N'd6443fcd-3b74-4bdf-a13c-a422385be0d9', N'0245508217', 0, 0, NULL, 1, 0)
GO
SET IDENTITY_INSERT [dbo].[Country] ON 
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (1, N'Afghanistan')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (2, N'Albania')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (3, N'Algeria')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (4, N'Andorra')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (5, N'Angola')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (6, N'Antigua and Barbuda')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (7, N'Argentina')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (8, N'Armenia')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (9, N'Australia')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (10, N'Austria')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (11, N'Azerbaijan')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (12, N'Bahamas')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (13, N'Bahrain')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (14, N'Bangladesh')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (15, N'Barbados')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (16, N'Belarus')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (17, N'Belgium')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (18, N'Belize')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (19, N'Benin')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (20, N'Bhutan')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (21, N'Bolivia')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (22, N'Bosnia and Herzegovina')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (23, N'Botswana')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (24, N'Brazil')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (25, N'Brunei')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (26, N'Bulgaria')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (27, N'Burkina Faso')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (28, N'Burundi')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (29, N'Cabo Verde')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (30, N'Cambodia')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (31, N'Cameroon')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (32, N'Canada')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (33, N'Central African Republic')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (34, N'Chad')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (35, N'Chile')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (36, N'China')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (37, N'Colombia')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (38, N'Comoros')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (39, N'Congo (Congo-Brazzaville)')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (40, N'Costa Rica')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (41, N'Croatia')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (42, N'Cuba')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (43, N'Cyprus')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (44, N'Czech Republic')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (45, N'Denmark')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (46, N'Djibouti')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (47, N'Dominica')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (48, N'Dominican Republic')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (49, N'East Timor (Timor-Leste)')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (50, N'Ecuador')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (51, N'Egypt')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (52, N'El Salvador')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (53, N'Equatorial Guinea')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (54, N'Eritrea')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (55, N'Estonia')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (56, N'Eswatini')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (57, N'Ethiopia')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (58, N'Fiji')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (59, N'Finland')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (60, N'France')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (61, N'Gabon')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (62, N'Gambia')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (63, N'Georgia')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (64, N'Germany')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (65, N'Ghana')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (66, N'Greece')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (67, N'Grenada')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (68, N'Guatemala')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (69, N'Guinea')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (70, N'Guinea-Bissau')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (71, N'Guyana')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (72, N'Haiti')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (73, N'Honduras')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (74, N'Hungary')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (75, N'Iceland')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (76, N'India')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (77, N'Indonesia')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (78, N'Iran')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (79, N'Iraq')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (80, N'Ireland')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (81, N'Israel')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (82, N'Italy')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (83, N'Jamaica')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (84, N'Japan')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (85, N'Jordan')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (86, N'Kazakhstan')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (87, N'Kenya')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (88, N'Kiribati')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (89, N'Korea, North')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (90, N'Korea, South')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (91, N'Kosovo')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (92, N'Kuwait')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (93, N'Kyrgyzstan')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (94, N'Laos')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (95, N'Latvia')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (96, N'Lebanon')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (97, N'Lesotho')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (98, N'Liberia')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (99, N'Libya')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (100, N'Liechtenstein')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (101, N'Lithuania')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (102, N'Luxembourg')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (103, N'Madagascar')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (104, N'Malawi')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (105, N'Malaysia')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (106, N'Maldives')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (107, N'Mali')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (108, N'Malta')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (109, N'Marshall Islands')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (110, N'Mauritania')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (111, N'Mauritius')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (112, N'Mexico')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (113, N'Micronesia')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (114, N'Moldova')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (115, N'Monaco')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (116, N'Mongolia')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (117, N'Montenegro')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (118, N'Morocco')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (119, N'Mozambique')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (120, N'Myanmar')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (121, N'Namibia')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (122, N'Nauru')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (123, N'Nepal')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (124, N'Netherlands')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (125, N'New Zealand')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (126, N'Nicaragua')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (127, N'Niger')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (128, N'Nigeria')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (129, N'North Macedonia')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (130, N'Norway')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (131, N'Oman')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (132, N'Pakistan')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (133, N'Palau')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (134, N'Panama')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (135, N'Papua New Guinea')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (136, N'Paraguay')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (137, N'Peru')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (138, N'Philippines')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (139, N'Poland')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (140, N'Portugal')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (141, N'Qatar')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (142, N'Romania')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (143, N'Russia')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (144, N'Rwanda')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (145, N'Saint Kitts and Nevis')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (146, N'Saint Lucia')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (147, N'Saint Vincent and the Grenadines')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (148, N'Samoa')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (149, N'San Marino')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (150, N'Sao Tome and Principe')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (151, N'Saudi Arabia')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (152, N'Senegal')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (153, N'Serbia')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (154, N'Seychelles')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (155, N'Sierra Leone')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (156, N'Singapore')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (157, N'Slovakia')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (158, N'Slovenia')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (159, N'Solomon Islands')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (160, N'Somalia')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (161, N'South Africa')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (162, N'South Sudan')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (163, N'Spain')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (164, N'Sri Lanka')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (165, N'Sudan')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (166, N'Suriname')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (167, N'Sweden')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (168, N'Switzerland')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (169, N'Syria')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (170, N'Tajikistan')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (171, N'Tanzania')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (172, N'Thailand')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (173, N'Togo')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (174, N'Tonga')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (175, N'Trinidad and Tobago')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (176, N'Tunisia')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (177, N'Turkey')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (178, N'Turkmenistan')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (179, N'Tuvalu')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (180, N'Uganda')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (181, N'Ukraine')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (182, N'United Arab Emirates')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (183, N'United Kingdom (UK)')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (184, N'United States of America (USA)')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (185, N'Uruguay')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (186, N'Uzbekistan')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (187, N'Vanuatu')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (188, N'Vatican City')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (189, N'Venezuela')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (190, N'Vietnam')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (191, N'Yemen')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (192, N'Zambia')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName]) VALUES (193, N'Zimbabwe')
GO
SET IDENTITY_INSERT [dbo].[Country] OFF
GO
SET IDENTITY_INSERT [dbo].[Player] ON 
GO
INSERT [dbo].[Player] ([PlayerId], [PlayerName], [JerseyNumber], [PositionId], [NationalityId], [BirthDate], [BirthPlace], [Weight], [Height], [ClubDebutDate], [QualityDescription], [CreateDate], [CreateBy]) VALUES (1, N' Marc-André Ter Stegen', 1, 1, 64, CAST(N'1992-04-30T00:00:00.000' AS DateTime), N'Mönchengladbach, Germany', 85, 187, NULL, N'A keeper with great reflexes and also excellent with the ball at his feet

Marc-André ter Stegen signed for FC Barcelona in the summer of 2014 from Borussia Mönchengladbach. Born on 30 April 1992 in Mönchengladbach itself, the German did not take long to make his name as one Europe’s most promising young goalkeepers.', CAST(N'2023-11-09T19:06:25.340' AS DateTime), NULL)
GO
INSERT [dbo].[Player] ([PlayerId], [PlayerName], [JerseyNumber], [PositionId], [NationalityId], [BirthDate], [BirthPlace], [Weight], [Height], [ClubDebutDate], [QualityDescription], [CreateDate], [CreateBy]) VALUES (2, N'Iñaki Peña', 13, 1, 163, CAST(N'1999-03-02T00:00:00.000' AS DateTime), N'Alicante, Spain', 78, 184, NULL, N'Iñaki Peña is a Barça-style goalkeeper: he reads the game well and can use his feet well

Iñaki Peña signed for Alicante CF when he was just five years old. In 2009, Villarreal CF was interested in the precocious player, where he began to stand out, especially in 2011, when he was chosen as the best goalkeeper of the Arona tournament. Barça had its eye on him and a year later he came to the Masia.', CAST(N'2023-11-09T19:21:41.793' AS DateTime), NULL)
GO
INSERT [dbo].[Player] ([PlayerId], [PlayerName], [JerseyNumber], [PositionId], [NationalityId], [BirthDate], [BirthPlace], [Weight], [Height], [ClubDebutDate], [QualityDescription], [CreateDate], [CreateBy]) VALUES (3, N'João Cancelo', 2, 2, 140, CAST(N'1994-05-27T00:00:00.000' AS DateTime), N'Barreiro, Portugal', 74, 182, NULL, N'Supreme right back who had already excelled at a number of top clubs before his loan to Barça', CAST(N'2023-11-09T19:27:12.213' AS DateTime), NULL)
GO
INSERT [dbo].[Player] ([PlayerId], [PlayerName], [JerseyNumber], [PositionId], [NationalityId], [BirthDate], [BirthPlace], [Weight], [Height], [ClubDebutDate], [QualityDescription], [CreateDate], [CreateBy]) VALUES (4, N'Alejandro Balde', 3, 2, 163, CAST(N'2003-10-18T00:00:00.000' AS DateTime), N'Barcelona, Spain', 69, 175, NULL, N'Skill and strength make Alejandro Balde a fast, explosive fullback who can also support the attack', CAST(N'2023-11-09T19:29:04.440' AS DateTime), NULL)
GO
INSERT [dbo].[Player] ([PlayerId], [PlayerName], [JerseyNumber], [PositionId], [NationalityId], [BirthDate], [BirthPlace], [Weight], [Height], [ClubDebutDate], [QualityDescription], [CreateDate], [CreateBy]) VALUES (5, N'Ronald Araujo', 4, 2, 185, CAST(N'1999-03-07T00:00:00.000' AS DateTime), N'Rivera, Uruguay', 79, 188, NULL, N'Uruguayan defender, strong in the air and capable with the ball at his feet', CAST(N'2023-11-09T19:32:13.657' AS DateTime), NULL)
GO
INSERT [dbo].[Player] ([PlayerId], [PlayerName], [JerseyNumber], [PositionId], [NationalityId], [BirthDate], [BirthPlace], [Weight], [Height], [ClubDebutDate], [QualityDescription], [CreateDate], [CreateBy]) VALUES (6, N'Íñigo Martínez', 5, 2, 163, CAST(N'1991-05-17T00:00:00.000' AS DateTime), N'Bilbao, Spain', 76, 182, NULL, N'After more than a decade playing at the top level in Spain, Iñigo has proven his qualities at centre back', CAST(N'2023-11-09T19:34:24.440' AS DateTime), NULL)
GO
INSERT [dbo].[Player] ([PlayerId], [PlayerName], [JerseyNumber], [PositionId], [NationalityId], [BirthDate], [BirthPlace], [Weight], [Height], [ClubDebutDate], [QualityDescription], [CreateDate], [CreateBy]) VALUES (7, N'Andreas Christensen', 15, 2, 45, CAST(N'1996-10-04T00:00:00.000' AS DateTime), N'Lillerod, Denmark', 82, 187, NULL, N'Centre back with brilliant passing skills and a tremendous reading of the game. Fabulous and finding the right route to get the ball forward', CAST(N'2023-11-09T19:35:37.820' AS DateTime), NULL)
GO
INSERT [dbo].[Player] ([PlayerId], [PlayerName], [JerseyNumber], [PositionId], [NationalityId], [BirthDate], [BirthPlace], [Weight], [Height], [ClubDebutDate], [QualityDescription], [CreateDate], [CreateBy]) VALUES (8, N'Marcos Alonso', 17, 2, 163, CAST(N'1990-12-28T00:00:00.000' AS DateTime), N'Madrid, Spain', 84, 188, NULL, N'Skilful full back or sweeper with a fine left foot', CAST(N'2023-11-09T19:36:59.083' AS DateTime), NULL)
GO
INSERT [dbo].[Player] ([PlayerId], [PlayerName], [JerseyNumber], [PositionId], [NationalityId], [BirthDate], [BirthPlace], [Weight], [Height], [ClubDebutDate], [QualityDescription], [CreateDate], [CreateBy]) VALUES (9, N'Jules Kounde', 23, 2, 60, CAST(N'1998-11-12T00:00:00.000' AS DateTime), N'Paris, France', 75, 180, NULL, N'Jules Kounde is known for being a very fast defender, quick to intercept, and being good on the ball', CAST(N'2023-11-09T19:38:36.897' AS DateTime), NULL)
GO
INSERT [dbo].[Player] ([PlayerId], [PlayerName], [JerseyNumber], [PositionId], [NationalityId], [BirthDate], [BirthPlace], [Weight], [Height], [ClubDebutDate], [QualityDescription], [CreateDate], [CreateBy]) VALUES (10, N' Gavi', 6, 3, 163, CAST(N'2004-08-05T00:00:00.000' AS DateTime), N'Los Palacios y Villafranca, Spain', 70, 173, NULL, N'Technically gifted midfielder with plenty of character and a good reader of the game', CAST(N'2023-11-09T19:40:57.920' AS DateTime), NULL)
GO
INSERT [dbo].[Player] ([PlayerId], [PlayerName], [JerseyNumber], [PositionId], [NationalityId], [BirthDate], [BirthPlace], [Weight], [Height], [ClubDebutDate], [QualityDescription], [CreateDate], [CreateBy]) VALUES (11, N' Pedri', 8, 3, 163, CAST(N'2002-11-25T00:00:00.000' AS DateTime), N'Tenerife, Spain', 60, 174, CAST(N'2020-09-27' AS Date), N'The player enjoys playing on the front foot, driving at his direct opponent and having his passes break the defensive lines', CAST(N'2023-11-09T19:42:20.387' AS DateTime), NULL)
GO
INSERT [dbo].[Player] ([PlayerId], [PlayerName], [JerseyNumber], [PositionId], [NationalityId], [BirthDate], [BirthPlace], [Weight], [Height], [ClubDebutDate], [QualityDescription], [CreateDate], [CreateBy]) VALUES (12, N'Oriol Romeu', 18, 3, 163, CAST(N'1991-09-24T00:00:00.000' AS DateTime), N'Ulldecona, Spain', 83, 183, NULL, N'A powerful and very talented player with an intimate knowledge of the Barça playing style', CAST(N'2023-11-09T19:43:35.267' AS DateTime), NULL)
GO
INSERT [dbo].[Player] ([PlayerId], [PlayerName], [JerseyNumber], [PositionId], [NationalityId], [BirthDate], [BirthPlace], [Weight], [Height], [ClubDebutDate], [QualityDescription], [CreateDate], [CreateBy]) VALUES (13, N'Sergi Roberto', 20, 3, 163, CAST(N'1992-07-02T00:00:00.000' AS DateTime), N'Reus, Spain', 68, 178, CAST(N'2010-11-10' AS Date), N'Sergi Roberto is one of the most versatile players in the squad and is known for his ability to get into the box', CAST(N'2023-11-09T19:45:05.060' AS DateTime), NULL)
GO
INSERT [dbo].[Player] ([PlayerId], [PlayerName], [JerseyNumber], [PositionId], [NationalityId], [BirthDate], [BirthPlace], [Weight], [Height], [ClubDebutDate], [QualityDescription], [CreateDate], [CreateBy]) VALUES (14, N'Frenkie de Jong', 21, 3, 124, CAST(N'1997-05-12T00:00:00.000' AS DateTime), N'Arkel, Netherlands', 74, 181, CAST(N'2019-08-16' AS Date), N'His versatility, energy and great vision have allowed him to continue developing his skills in any midfield position', CAST(N'2023-11-09T19:46:21.887' AS DateTime), NULL)
GO
INSERT [dbo].[Player] ([PlayerId], [PlayerName], [JerseyNumber], [PositionId], [NationalityId], [BirthDate], [BirthPlace], [Weight], [Height], [ClubDebutDate], [QualityDescription], [CreateDate], [CreateBy]) VALUES (15, N'Ilkay Gündogan', 22, 3, 64, CAST(N'1990-10-24T00:00:00.000' AS DateTime), N'Gelsenkirchen, Germany', 80, 180, NULL, N'With extensive experience of elite football, Gündoğan is an all-round midfielder who has also developed into a great goalscorer', CAST(N'2023-11-09T19:48:27.570' AS DateTime), NULL)
GO
INSERT [dbo].[Player] ([PlayerId], [PlayerName], [JerseyNumber], [PositionId], [NationalityId], [BirthDate], [BirthPlace], [Weight], [Height], [ClubDebutDate], [QualityDescription], [CreateDate], [CreateBy]) VALUES (16, N'Ferran Torres', 7, 4, 163, CAST(N'2000-02-29T00:00:00.000' AS DateTime), N'Foios, Spain', 77, 184, CAST(N'2022-12-01' AS Date), N'He brings together various talents which allow him to operate anywhere up front', CAST(N'2023-11-09T19:50:06.870' AS DateTime), NULL)
GO
INSERT [dbo].[Player] ([PlayerId], [PlayerName], [JerseyNumber], [PositionId], [NationalityId], [BirthDate], [BirthPlace], [Weight], [Height], [ClubDebutDate], [QualityDescription], [CreateDate], [CreateBy]) VALUES (17, N'Robert Lewandowski', 9, 4, 139, CAST(N'1988-08-21T00:00:00.000' AS DateTime), N'Warszawa, Poland', 81, 185, NULL, N'One of the best strikers ever, the Polish forward''s stats speak for themselves', CAST(N'2023-11-09T19:51:11.133' AS DateTime), NULL)
GO
INSERT [dbo].[Player] ([PlayerId], [PlayerName], [JerseyNumber], [PositionId], [NationalityId], [BirthDate], [BirthPlace], [Weight], [Height], [ClubDebutDate], [QualityDescription], [CreateDate], [CreateBy]) VALUES (18, N' Raphinha', 11, 4, 24, CAST(N'1996-12-14T00:00:00.000' AS DateTime), NULL, 68, 176, NULL, N'A technically gifted, skill winger with great dribbling skills and combination play', CAST(N'2023-11-09T19:52:11.253' AS DateTime), NULL)
GO
INSERT [dbo].[Player] ([PlayerId], [PlayerName], [JerseyNumber], [PositionId], [NationalityId], [BirthDate], [BirthPlace], [Weight], [Height], [ClubDebutDate], [QualityDescription], [CreateDate], [CreateBy]) VALUES (19, N'João Félix', 14, 4, 140, CAST(N'1999-11-10T00:00:00.000' AS DateTime), N'Viseu, Portugal', 70, 181, NULL, N'Lights up football with the creative flair and talent in the final third of the field', CAST(N'2023-11-09T19:53:19.623' AS DateTime), NULL)
GO
SET IDENTITY_INSERT [dbo].[Player] OFF
GO
SET IDENTITY_INSERT [dbo].[Position] ON 
GO
INSERT [dbo].[Position] ([PositionId], [PositionName]) VALUES (1, N'Goalkeeper')
GO
INSERT [dbo].[Position] ([PositionId], [PositionName]) VALUES (2, N'Defender')
GO
INSERT [dbo].[Position] ([PositionId], [PositionName]) VALUES (3, N'Midfielders')
GO
INSERT [dbo].[Position] ([PositionId], [PositionName]) VALUES (4, N'Forwards')
GO
INSERT [dbo].[Position] ([PositionId], [PositionName]) VALUES (5, N'Coaching Staff')
GO
SET IDENTITY_INSERT [dbo].[Position] OFF
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetRoleClaims_RoleId]    Script Date: 11/9/2023 10:47:14 PM ******/
CREATE NONCLUSTERED INDEX [IX_AspNetRoleClaims_RoleId] ON [dbo].[AspNetRoleClaims]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [RoleNameIndex]    Script Date: 11/9/2023 10:47:14 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
(
	[NormalizedName] ASC
)
WHERE ([NormalizedName] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserClaims_UserId]    Script Date: 11/9/2023 10:47:14 PM ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserClaims_UserId] ON [dbo].[AspNetUserClaims]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserLogins_UserId]    Script Date: 11/9/2023 10:47:14 PM ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserLogins_UserId] ON [dbo].[AspNetUserLogins]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserRoles_RoleId]    Script Date: 11/9/2023 10:47:14 PM ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserRoles_RoleId] ON [dbo].[AspNetUserRoles]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [EmailIndex]    Script Date: 11/9/2023 10:47:14 PM ******/
CREATE NONCLUSTERED INDEX [EmailIndex] ON [dbo].[AspNetUsers]
(
	[NormalizedEmail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UserNameIndex]    Script Date: 11/9/2023 10:47:14 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
(
	[NormalizedUserName] ASC
)
WHERE ([NormalizedUserName] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Player] ADD  CONSTRAINT [DF_Player_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[AspNetRoleClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetRoleClaims] CHECK CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserTokens]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserTokens] CHECK CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId]
GO
USE [master]
GO
ALTER DATABASE [FCB_Demo_DB] SET  READ_WRITE 
GO
