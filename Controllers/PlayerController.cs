﻿using DemoProject.Entities;
using DemoProject.Models;
using DemoProject.Services.Player;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DemoProject.Controllers
{
    public class PlayerController : Controller
    {
        private readonly IPlayerService _service;

        public PlayerController(IPlayerService service)
        {
            _service = service;
        }

        public async Task<IActionResult> Index()
        {
            return View(await _service.GetAllPlayersAsync());
        }

        public async Task<IActionResult> GetDropdownPositionData()
        {
            // Fetch data from the database (replace this with your actual data retrieval logic)
            var data = await _service.GetAllPositionsAsync();

            var dropdownData = data.Select(item => new DropDownItem
            {
                Value = Convert.ToInt32(item.PositionId), 
                Text = item.PositionName 
            })
            .ToList();

            return Json(dropdownData);
        }

        public async Task<IActionResult> GetDropdownCountryData()
        {
            // Fetch data from the database (replace this with your actual data retrieval logic)
            var data = await _service.GetAllCountriesAsync();

            var dropdownData = data.Select(item => new DropDownItem
            {
                Value = Convert.ToInt32(item.CountryId),
                Text = item.CountryName
            })
            .ToList();

            return Json(dropdownData);
        }

        public IActionResult Create()
        {
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PlayerId,PlayerName,JerseyNumber,PositionId," +
            "NationalityId,BirthDate,BirthPlace,Weight,Height,ClubDebutDate,QualityDescription,CreateDate,CreateBy")] Player model)
        {
            if (ModelState.IsValid)
            {
                await _service.AddPlayerAsync(model);

                return RedirectToAction(nameof(Index), new { id = model.PlayerId });
            }
            return View(model);
        }

        public async Task<IActionResult> Edit(long id)
        {
            if (id == 0)
            {
                return NotFound();
            }

            var data = await _service.GetPlayerByIdAsync(id);

            if (data == null)
            {
                return NotFound();
            }

            return View(data);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("PlayerId,PlayerName,JerseyNumber,PositionId," +
            "NationalityId,BirthDate,BirthPlace,Weight,Height,ClubDebutDate,QualityDescription,CreateDate,CreateBy")] Player model)
        {
            if (id != model.PlayerId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _service.UpdatePlayerAsync(model);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await _service.PlayerExistsAsync(model.PlayerId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            return View(model);
        }

        public async Task<IActionResult> Delete(long id)
        {
            if (id == 0)
            {
                return NotFound();
            }

            var data = await _service.GetPlayerByIdAsync(id);

            if (data == null)
            {
                return NotFound();
            }

            return View(data);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            await _service.DeletePlayerAsync(id);

            return RedirectToAction(nameof(Index));
        }
    }
}
